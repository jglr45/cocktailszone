## Wichtig

Um sicher zu sein, dass alles funktioniert <https://cocktailszone.herokuapp.com> verwenden. Chrome unterstüzt die
Standortbestimmung nur bei sicheren Verbindungen.

## CocktailsZone

CocktailsZone biete eine breite Auswahl an Cocktails.
Zu Beginn der App sieht man das Wetter zur momentanen Zeit, in zwei Stunden und in 5 Stunden, damit man planen kann, wo 
man seine Cocktails genießen kann.

Man kann aus vielen verschiedenen Zutaten wählen und sich dazu passende Coktails anzeigen lassen. Zusätzlich kann man
man die Cocktails auch nach Kategorien filtern lassen.

Information zu den APIs:

In der App wurden zwei APIs verwendet. Zum einen "The Cocktail db", welche die Coktails und Zutaten liefert und zum
anderen die "openweathermap" API, welche die Wetterdaten liefert. 

## Installation

Um die App zu installieren genötigt man lediglich GIT und NPM(Node Packet Manager).

Zuerst schaut man ob GIT installiert ist mit "git --version". Ist GIT nicht installiert, kannst man es auf der Website
GIT kostenlos herunterladen.

Nun schaut man ob Node und NPM bereits installiert sind. Dazu gibt man "node --version" und "npm --version" in die
Konsole ein. Kommt bei mindestens einer der beiden Eingaben ein fehler, so kann man auf der Website von Node.js die
neuste Version herunterladen und installieren.

Nun kann man das Projekt mit GIT klonen. Entweder mit SSH über "git@git.thm.de:jglr45/cocktailszone.git" oder mit HTTPS
über "https://git.thm.de/jglr45/cocktailszone.git".

Nachdem die App vollständig heruntergeladen ist, muss man "npm start" innerhalb der App in der Konsole ausführen. Danach
ist die App unter <http://localhost:3000> erreichbar.