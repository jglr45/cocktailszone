var request = require("request");

class TheCocktailDb {

    get alcoholicFilters() {
        return this._alcoholicFilters
    }

    get ingredientsList() {
        return this._ingredientsList
    }

    get glasList() {
        return this._glasList
    }

    get categories() {
        return this._categories
    }


    constructor() {
        this._initAlcoholicFilters()
        this._initIngredientsList()
        this._initCategories()
    }

    _initCategories() {
        const reference = this
        request.get({
            url: 'https://the-cocktail-db.p.rapidapi.com/list.php',
            qs: {c: 'list'},
            json: true,
            headers: {
                'x-rapidapi-host': 'the-cocktail-db.p.rapidapi.com',
                'x-rapidapi-key': '98efdfe3c5msh2b540c93e16ae25p114aebjsn540166826e14',
                useQueryString: true
            }
        }, function (error, response, body) {
            if (error) console.error(error);

            reference._categories = body.drinks.sort((a, b) => {
                return a.strCategory.localeCompare(b.strCategory)
            })
        });
    }

    _initIngredientsList() {
        const reference = this
        request.get({
            url: 'https://the-cocktail-db.p.rapidapi.com/list.php',
            qs: {i: 'list'},
            json: true,
            headers: {
                'x-rapidapi-host': 'the-cocktail-db.p.rapidapi.com',
                'x-rapidapi-key': '98efdfe3c5msh2b540c93e16ae25p114aebjsn540166826e14',
                useQueryString: true
            }
        }, function (error, response, body) {
            if (error) console.error(error);

            reference._ingredientsList = body.drinks.sort((a, b) => {
                return a.strIngredient1.localeCompare(b.strIngredient1)
            })
        });
    }


    _initAlcoholicFilters() {
        const reference = this
        request.get({
            url: 'https://the-cocktail-db.p.rapidapi.com/list.php',
            qs: {a: 'list'},
            json: true,
            headers: {
                'x-rapidapi-host': 'the-cocktail-db.p.rapidapi.com',
                'x-rapidapi-key': '98efdfe3c5msh2b540c93e16ae25p114aebjsn540166826e14',
                useQueryString: true
            }
        }, function (error, response, body) {
            if (error) console.error(error);

            reference._alcoholicFilters = body.drinks
        });
    }

    getIngedientByName(callback, name) {
        request.get({
            url: 'https://the-cocktail-db.p.rapidapi.com/search.php',
            qs: {i: name},
            json: true,
            headers: {
                'x-rapidapi-host': 'the-cocktail-db.p.rapidapi.com',
                'x-rapidapi-key': '98efdfe3c5msh2b540c93e16ae25p114aebjsn540166826e14',
                useQueryString: true
            }
        }, function (error, response, body) {
            if (error) console.error(error)

            callback(body.ingredients)
        });
    }

    getCocktailsByIngredient(callback, ingredient) {
        request.get({
            url: 'https://the-cocktail-db.p.rapidapi.com/filter.php',
            qs: {i: ingredient},
            json: true,
            headers: {
                'x-rapidapi-host': 'the-cocktail-db.p.rapidapi.com',
                'x-rapidapi-key': '98efdfe3c5msh2b540c93e16ae25p114aebjsn540166826e14',
                useQueryString: true
            }
        }, function (error, response, body) {
            if (error) console.error(error)

            callback(body.drinks)
        });
    }

    getCocktailsByCategory(callback, category) {
        request.get({
            url: 'https://the-cocktail-db.p.rapidapi.com/filter.php',
            qs: {c: category},
            json: true,
            headers: {
                'x-rapidapi-host': 'the-cocktail-db.p.rapidapi.com',
                'x-rapidapi-key': '98efdfe3c5msh2b540c93e16ae25p114aebjsn540166826e14',
                useQueryString: true
            }
        }, function (error, response, body) {
            if (error) console.error(error)

            callback(body.drinks)
        });
    }

    getCocktailsByAlcohol(callback, alcohol) {
        request.get({
            url: 'https://the-cocktail-db.p.rapidapi.com/filter.php',
            qs: {a: alcohol},
            json: true,
            headers: {
                'x-rapidapi-host': 'the-cocktail-db.p.rapidapi.com',
                'x-rapidapi-key': '98efdfe3c5msh2b540c93e16ae25p114aebjsn540166826e14',
                useQueryString: true
            }
        }, function (error, response, body) {
            if (error) console.error(error)

            callback(body.drinks)
        });
    }

    getDrinkById(callback, id) {
        request.get({
            url: 'https://the-cocktail-db.p.rapidapi.com/lookup.php',
            qs: {i: id},
            json: true,
            headers: {
                'x-rapidapi-host': 'the-cocktail-db.p.rapidapi.com',
                'x-rapidapi-key': '98efdfe3c5msh2b540c93e16ae25p114aebjsn540166826e14',
                useQueryString: true
            }
        }, function (error, response, body) {
            if (error) console.error(error)

            callback(body.drinks)
        });
    }

    getRandom(callback) {
        request.get({
            url: 'https://the-cocktail-db.p.rapidapi.com/random.php',
            json: true,
            headers: {
                'x-rapidapi-host': 'the-cocktail-db.p.rapidapi.com',
                'x-rapidapi-key': '98efdfe3c5msh2b540c93e16ae25p114aebjsn540166826e14',
                useQueryString: true
            }
        }, function (error, response, body) {
            if (error) console.error(error);

            callback(body.drinks)
        });
    }


}

module.exports = new TheCocktailDb()
