var request = require('request')

class weather {

    getWeatherByGeoCode(callback, lat, lon) {
        request.get({
            url: 'https://api.openweathermap.org/data/2.5/onecall?lat='+lat+'&lon='+lon+'&exclude=current,minutely,daily&appid=48a6a925deef5f3d75c44cc1e179b216',
            json: true,
        }, function (error, response, body) {
            if (error) console.error(error)

            const result = []
            let current = body.hourly
            for(let i in current) {
                const data = current[i].weather[0]
                result.push([data.icon, data.main, data.description])
            }
            callback(result)
        })
    }
}

module.exports = new weather()
