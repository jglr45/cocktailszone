var express = require('express');
var router = express.Router();
const theCocktailDb = require('../lib/the-cocktail-db')
const weather = require('../lib/weather')

async function executed(res) {
    const [v1, v2, v3] = await Promise.all([new Promise((resolve) => theCocktailDb.getRandom((data) => resolve(data[0]))), new Promise((resolve) => theCocktailDb.getRandom((data) => resolve(data[0]))), new Promise((resolve) => theCocktailDb.getRandom((data) => resolve(data[0])))])
    res.render('index', {startingPage: [v1, v2, v3], alcfilters: theCocktailDb.alcoholicFilters})
}

/* GET home page. */
router.get('/', function (req, res, next) {
    executed(res)
});

router.get('/query', function (req, res) {
    const lon = req.query.lon
    const lat = req.query.lat
    weather.getWeatherByGeoCode((data) => {
        res.render('weatherPart',{data: data})
    }, lat, lon)
})

router.get('/ingredients', function (req, res, next) {
    res.render('ingredients', {ingredients: theCocktailDb.ingredientsList, alcfilters: theCocktailDb.alcoholicFilters})
})

router.get('/searchedCocktailsAlcohol/:alcohol', function (req, res, next) {
    const alcohol = req.params.alcohol
    theCocktailDb.getCocktailsByAlcohol((data) => {
        res.render('searchedCocktails', {cocktails: data, alcfilters: theCocktailDb.alcoholicFilters})
    }, alcohol)
})

router.get('/searchedCocktailsCategory/:category', function (req, res, next) {
    const category = req.params.category.replace(/_/gm, "/")
    theCocktailDb.getCocktailsByCategory((data) => {
        res.render('searchedCocktails', {cocktails: data, alcfilters: theCocktailDb.alcoholicFilters})
    }, category)
})

router.get('/searchedCocktails/:ingredient', function (req, res, next) {
    const ingredient = req.params.ingredient
    theCocktailDb.getCocktailsByIngredient((data) => {
        res.render('searchedCocktails', {cocktails: data, alcfilters: theCocktailDb.alcoholicFilters})
    }, ingredient)
})

router.get('/cocktail/:id', function (req, res, next) {
    const id = req.params.id
    theCocktailDb.getDrinkById((data) => {
        res.render('cocktail', {cocktail: data, alcfilters: theCocktailDb.alcoholicFilters})
    }, id)
})

router.get('/searchedIngredient/:name', function (req, res, next) {
    const name = req.params.name
    theCocktailDb.getIngedientByName((data) => {
        res.render('searchedIngredient', {ingredient: data, alcfilters: theCocktailDb.alcoholicFilters})
    }, name)
})

router.get('/glasses', function (req, res, next) {
    res.render('glasses', {glasses: theCocktailDb.glasList, alcfilters: theCocktailDb.alcoholicFilters})
})

router.get('/categories', function (req, res, next) {
    res.render('categories', {categories: theCocktailDb.categories, alcfilters: theCocktailDb.alcoholicFilters})
})

module.exports = router;